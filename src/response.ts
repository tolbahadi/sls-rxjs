import * as Events from 'events';
import { LambdaCallback } from './aws-lambda';
import * as cookie from 'cookie';
import * as _ from 'lodash';

export class Response extends Events {
  private lambdaCallbak: LambdaCallback;
  finished: boolean;

  constructor(callback: LambdaCallback) {
    super();
    this.lambdaCallbak = callback;
    this.finished = false;
  }

  send(data: any) {
    if (!this.finished) {
      this.lambdaCallbak(null, data);
      this.finished = true;
      this.emit('finish');
    } else {
      const error = new Error('Response has been sent already!');
      // this.emit('error', error);
      throw error;
    }
  }

  fail(error: Error) {
    this.lambdaCallbak(error);
  }
}

const getNoCacheHeader = () => ({
  'Cache-Control': 'private, max-age=0, no-cache, no-store, must-revalidate',
  Pragma: 'no-cache',
  Expires: '0',
});

export class HttpResponse extends Response {
  private headers: { [index: string]: string } = {};

  private cookies: { [index: string]: string } = {};

  private statusCode: number = 200;

  noCache = true;

  set(name: string, value: string) {
    this.headers[name] = value;
    return this;
  }

  setCookie(name: string, value: string) {
    this.cookies[name] = value;
  }

  status(value: number) {
    this.statusCode = value;
    return this;
  }

  getStatus() {
    return this.statusCode;
  }

  send(data: any, isBase64Encoded = false) {
    let headers = this.headers;
    if (this.noCache) {
      headers = {...headers, ...getNoCacheHeader()};
    }
    const response = {
      statusCode: this.statusCode,
      headers,
      body: '',
      isBase64Encoded,
    };

    if (!_.isEmpty(this.cookies)) {
      response.headers['Set-Cookie'] = _.map(this.cookies, (val, key) => cookie.serialize(key, val)).join('; ');
    }

    if (typeof data === 'string') {
      response.headers['Content-Type'] = response.headers['Content-Type'] || 'text/html';
      response.body = data;
    } else {
      response.headers['Content-Type'] = response.headers['Content-Type'] || 'application/json';
      response.body = JSON.stringify(data);
    }

    super.send(response);
  }
}
