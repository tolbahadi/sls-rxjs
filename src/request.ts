'use strict';

import { each, isString, pick, reduce, assign } from 'lodash';

import { ILambdaHttpEventContext, ILambdaHttpEvent } from './aws-lambda';

import * as querystring from 'querystring';
import * as cookie from 'cookie';

export interface StringDict {
  [index: string]: string;
}

export class HttpRequest {
  public query: StringDict = {};
  public params: StringDict = {};
  public headers: StringDict = {};
  public cookies: StringDict = {};
  public lang: string;
  public resource: string;
  public path: string;
  public httpMethod: string;
  public context: ILambdaHttpEventContext;
  public body: any = {};
  public data: { [key: string]: any};

  constructor(event: ILambdaHttpEvent) {
    // console.log(JSON.stringify(event, null, 2))
    assign(this, pick(event, [
      'resource', 'path', 'httpMethod',
    ]));

    this.data = {};

    this.context = event.requestContext;

    each(['query', 'params'], (name) => {
      const value = name === 'query' ? event.queryStringParameters : event.pathParameters;
      const start: { [index: string]: string } = {};
      if (value) {
        let parsed = reduce(value, (memo, val, key) => {
          memo[key] = decodeURI(<string>val);
          return memo;
        }, start);

        name === 'query' ? this.query = parsed : this.params = parsed;
      }
    });

    if (event.headers) {
      this.headers = {};
      each(event.headers, (value, key) => {
        this.headers[key.toLocaleLowerCase()] = value;
      });
    }

    if (this.headers['content-type'] === 'application/json') {
      try {
        this.body = JSON.parse(event.body);
      } catch(err) {
        err = null;
      }
    } else {
      this.body = event.body;
    }

    if (this.headers['cookie']) {
      this.cookies = cookie.parse(this.headers['cookie']);
    }

    if (event.path) {
      this.path = decodeURI(event.path);
    }

    this.lang = getLang(this);
  }

  set(key: string, val: any) {
    this.data[key] = val;
  }

  get(key: string) {
    return this.data[key];
  }
}

const langs: any = {
  ar: 'ar',
  fr: 'fr',
};

function getLang(request: any) {
  let lang = '';

  if (request.query.lang && langs[request.query.lang.toLowerCase()]) {
    lang = langs[request.query.lang.toLowerCase()];
  } else if (request.cookies.lang && langs[request.cookies.lang.toLowerCase()]) {
    lang = langs[request.cookies.lang.toLowerCase()];
  }

  return lang;
}

// sample request
// {
//     "resource": "/feed/{id}",
//     "path": "/feed/hady",
//     "httpMethod": "GET",
//     "headers": {
//         "Accept": "*/*",
//         "Accept-Encoding": "gzip, deflate, sdch, br",
//         "Accept-Language": "en-US,en;q=0.8,ar;q=0.6",
//         "Cache-Control": "no-cache",
//         "CloudFront-Forwarded-Proto": "https",
//         "CloudFront-Is-Desktop-Viewer": "true",
//         "CloudFront-Is-Mobile-Viewer": "false",
//         "CloudFront-Is-SmartTV-Viewer": "false",
//         "CloudFront-Is-Tablet-Viewer": "false",
//         "CloudFront-Viewer-Country": "US",
//         "Content-Type": "application/json",
//         "Cookie": "_ga=GA1.4.1655139826.1459473403",
//         "Host": "t40r1eblp2.execute-api.us-east-1.amazonaws.com",
//         "User-Agent":
// "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36",
//         "Via": "1.1 d97deeb2385556a78005515cfaba11f9.cloudfront.net (CloudFront)",
//         "X-Amz-Cf-Id": "G6oaVTBhAPX3Q4l451oenRQtJlk6_c1j-nhY4K3YfnSHeB6lwFPwRA==",
//         "X-Forwarded-For": "69.123.202.197, 204.246.180.73",
//         "X-Forwarded-Port": "443",
//         "X-Forwarded-Proto": "https"
//     },
//     "queryStringParameters": null,
//     "pathParameters": {
//         "id": "hady"
//     },
//     "stageVariables": null,
//     "requestContext": {
//         "accountId": "770963267059",
//         "resourceId": "uxr2m8",
//         "stage": "dev",
//         "requestId": "ba2908ae-8b73-11e6-9505-b3230512abe2",
//         "identity": {
//             "cognitoIdentityPoolId": null,
//             "accountId": null,
//             "cognitoIdentityId": null,
//             "caller": null,
//             "apiKey": null,
//             "sourceIp": "69.123.202.197",
//             "cognitoAuthenticationType": null,
//             "cognitoAuthenticationProvider": null,
//             "userArn": null,
// "userAgent":
//   "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36",
//             "user": null
//         },
//         "resourcePath": "/feed/{id}",
//         "httpMethod": "GET",
//         "apiId": "t40r1eblp2"
//     },
//     "body": null
// }
