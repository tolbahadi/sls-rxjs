import { isString, isFunction, assign, isEmpty } from 'lodash';
import { Observable, Observer, Subject } from '@reactivex/rxjs';
import { HttpRequest } from './request';
import { HttpResponse } from './response';
import * as pathToRegexp from 'path-to-regexp';

// const subject = new Subject();

// function trigger(request: { method: 'GET' | 'POST', route: string}) {
//   subject.next(request);
// }

export interface RouteHandlerCallback {
  (req: HttpRequest, res: HttpResponse, next: Function, param?: string): void;
}

export type Method = 'GET' | 'POST' | 'PUT' | 'DELETE' | 'ANY';

export type RouteHandler = RouteHandlerCallback | Router;

export interface Middleware {
  method: Method;
  path: string;
  handler: RouteHandler;
  regex: RegExp;
  keys: pathToRegexp.Key[];
}

interface IResponseError extends Error {
  code: number;
}

export class Router {
  handlers: Middleware[] = [];
  mountPrefix = '';
  errorHandler: (error: any, req: HttpRequest, res: HttpResponse) => void;

  // addHandler(method: Method, handler: RouteHandler)
  addHandler(method: Method, path: string | RouteHandler, handler?: RouteHandler) {
    if (!isString(path)) {
      handler = <RouteHandler>path;
      path = '*';
    }

    if (handler instanceof Router) {
      path = path === '*' ? '' : path;
      handler.mountPrefix = this.mountPrefix + path;
    }

    const keys: pathToRegexp.Key[] = [];

    const regex = pathToRegexp(path, keys);

    this.handlers.push({
      method,
      path,
      regex,
      keys,
      handler,
    });

    return this;
  }

  error(error: any, req: HttpRequest, res: HttpResponse) {
    if (this.errorHandler != null) {
      this.errorHandler(error, req, res);
    } else {
      throw new Error('Error handler not registered.');
    }
  }

  get(path: string | RouteHandler, handler?: RouteHandler) {
    return this.addHandler('GET', path, handler);
  }

  put(path: string | RouteHandler, handler?: RouteHandler) {
    return this.addHandler('PUT', path, handler);
  }

  post(path: string | RouteHandler, handler?: RouteHandler) {
    return this.addHandler('POST', path, handler);
  }

  delete(path: string | RouteHandler, handler?: RouteHandler) {
    return this.addHandler('DELETE', path, handler);
  }

  all(path: string | RouteHandler, handler?: RouteHandler) {
    return this.addHandler('ANY', path, handler);
  }

  use(path: string | RouteHandler, handler?: RouteHandler) {
    return this.addHandler('ANY', path, handler);
  }

  // param(path: string, handler: RouteHandler) {
  //   return this.addHandler('ANY', path, handler);
  // }

  _shouldCallMiddleware(req: HttpRequest, middleware: Middleware) {
    if (middleware.handler instanceof Router) {
      return req.path.replace(this.mountPrefix, '').indexOf(middleware.path) === 0 &&
            (middleware.method === req.httpMethod || middleware.method === 'ANY');
    }
    return middleware.regex.test(req.path.replace(this.mountPrefix, '')) &&
            (middleware.method === req.httpMethod || middleware.method === 'ANY');
  }

  _callMiddleware(req: HttpRequest, res: HttpResponse, middleware: Middleware, next: Function) {
    if (middleware.handler instanceof Router) {
      middleware.handler._processMiddleware(req, res, next);
      return;
    }

    const parts = middleware.regex.exec(req.path.replace(this.mountPrefix, ''));

    const params: { [name: string]: string } = {};

    if (parts) {
      for (let i = 1; i < parts.length; i++) {
        const key = middleware.keys[i - 1];
        const name = key.name;
        const value = decodeParam(parts[i]);

        if (value !== undefined && !Object.prototype.hasOwnProperty.call(params, name)) {
          params[name] = value;
        }
      }
    }

    if (!isEmpty(params)) {
      req.params = assign({}, req.params, params);
    }

    middleware.handler(req, res, next);
  }

  _processMiddleware(req: HttpRequest, res: HttpResponse, next: Function) {
    return Observable.create((observer: Observer<any>) => {
      let index = -1;

      const triggerNext = () => {
        index += 1;
        if (index >= this.handlers.length) {
          observer.complete();
          return;
        }

        const middleware = this.handlers[index];

        if (this._shouldCallMiddleware(req, middleware)) {
          this._callMiddleware(req, res, middleware, (error?: Error) => {
            if (error) {
              observer.error(error);
            } else {
              triggerNext();
            }
          });
        } else {
          triggerNext();
        }
      };

      triggerNext();
    })
    .subscribe(null, next, () => next());
  }
}

export class App extends Router {
  listen(source: Observable<{ req: HttpRequest, res: HttpResponse }>) {
    source
    .map(({req, res}) => {
      this._processMiddleware(req, res, (error: Error) => {
        if (error) {
          if (this.errorHandler) {
            this.errorHandler(error, req, res);
          } else {
            res.status(500).send({ message: error.message });
          }
        } else {
          if (!res.finished) {
            res.status(404).send({ message: 'NotFound!' });
          }
        }
      });
    })
    .subscribe();
  }
}

/**
 * Decode param value.
 *
 * @param {string} val
 * @return {string}
 * @private
 */

function decodeParam(val: any) {
  if (typeof val !== 'string' || val.length === 0) {
    return val;
  }

  try {
    return decodeURIComponent(val);
  } catch (err) {
    if (err instanceof URIError) {
      err.message = 'Failed to decode param \'' + val + '\'';
      // err.status = err.statusCode = 400;
    }

    throw err;
  }
}

// const app = new App();

// const router = new Router();
// app.use('/', router);

// app.get('/', function(req, next) {
//   console.log('app get / handler');
// //   return next('stop');
// //   next();
// //   return Observable.throw('hady');
//   return Observable.empty();
// });

// app.get('/', function(req) {
//   console.log('app get / handler - 2');
//   return Observable.empty();

// });

// app.listen(subject);


// trigger({ method: 'GET', route: '/' });

// console.log('hey');
