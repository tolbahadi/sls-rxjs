import * as fs from 'fs';
import * as path from 'path';
import * as url from 'url';
import { Observable, Observer } from '@reactivex/rxjs';
import { HttpRequest } from './request';
import { HttpResponse } from './response';
import * as mime from 'mime';

export function staticRouteHandler(root: string, shouldCache: (path: string) => boolean) {
  return (req: HttpRequest, res: HttpResponse) => {
    const fullPath = path.join(root, url.parse(req.path).path.replace(/^\//, ''));

    const type = mime.lookup(fullPath);

    fs.readFile(fullPath, 'utf8', (error, content) => {
      if (error) {
        res.status(500).send({ message: error.message });
      } else {
        res.set('Content-Type', type);
        if (shouldCache(fullPath)) {
          res.set('Cache-Control', 'public, max-age=31104000');
          res.set('Expires', new Date(Date.now() + 1000 * 60 * 60 * 24 * 30 * 12).toISOString());
        }
        res.send(content);
      }
    });
  };

  function getFileContent(filePath: string): Observable<string> {
    return Observable.create((observer: Observer<string>) => {
      fs.readFile(path.join(root, filePath), 'utf8', (error, data) => {
        if (error) {
          observer.error(error);
        } else {
          observer.next(data);
          observer.complete();
        }
      });
    });
  }
}
