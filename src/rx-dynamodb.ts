import * as AWS from 'aws-sdk';
import { isArray, each, map, bind, assign } from 'lodash';
import { Observable } from '@reactivex/rxjs';

const config: AWS.DynamoDB.ClientConfiguration = {
  region: 'us-east-1', // TODO: Take region as env variable
  apiVersion: '2012-08-10',
};

if (process.env.LOCAL_DYNAMO_ENDPOINT) {
  config.endpoint = process.env.LOCAL_DYNAMO_ENDPOINT;
}

const service = new AWS.DynamoDB(config);

const dynamodb = new AWS.DynamoDB.DocumentClient({
  service,
});

interface IExpressionResult {
  expression: string;
  values: {
    [ key: string]: any;
  };

  names: {
    [ key: string]: string;
  };
}

function buildExpression(expressions: { [ key: string ]: any }): IExpressionResult;
function buildExpression(expressions: { [ key: string ]: any }[]): IExpressionResult;
function buildExpression(expressions: any): IExpressionResult {
  const result: IExpressionResult = {
    expression: '',
    names: {}, // { '#resetToken': 'resetToken'},
    values: {}, // { ':resetTokenValue': resetToken },
  };

  const expressionParts: string[] = [];

  if (!isArray(expressions)) {
    expressions = map(expressions, (value, key) => ({ key, value }));
  }

  each(expressions, (expression) => {
    const attributeName = `#${expression.key}Attr`;
    const attributeValue = `:${expression.key}Value`;

    result.names[attributeName] = expression.key;
    result.values[attributeValue] = expression.value;

    if (expression.fn) {
      expressionParts.push(`${expression.fn}(${attributeName}, ${attributeValue})`);
    } else {
      expressionParts.push(`${attributeName} ${expression.comp || '='} ${attributeValue}`);
    }
  });

  result.expression += expressionParts.join(', ');

  return result;
}

const $get = Observable.bindNodeCallback(
                (params: AWS.DynamoDB.DocumentClient.GetItemInput, cb: (err: any, data: any) => void) => 
                dynamodb.get(params, cb));
export function get(tableName: string, key: AWS.DynamoDB.DocumentClient.Key,
                    options: AWS.DynamoDB.DocumentClient.GetItemInput) {

  const params: AWS.DynamoDB.DocumentClient.GetItemInput = assign({
    TableName: tableName,
    Key: key,
  }, options);

  return $get(params);
}

const $delete = Observable.bindNodeCallback(
                (params: AWS.DynamoDB.DocumentClient.DeleteItemInput, cb: (err: any, data: any) => void) => 
                dynamodb.delete(params, cb));
export function deleteItem(tableName: string, key: AWS.DynamoDB.DocumentClient.Key,
                            options: AWS.DynamoDB.DocumentClient.DeleteItemInput) {

  const params: AWS.DynamoDB.DocumentClient.DeleteItemInput = assign({
    TableName: tableName,
    Key: key,
  }, options);

  return $delete(params);
}

const $put = Observable.bindNodeCallback((params: AWS.DynamoDB.DocumentClient.PutItemInput, cb:
                                              (err: any, data: any) => void) => 
              dynamodb.put(params, cb));
export function put(tableName: string, item: AWS.DynamoDB.DocumentClient.Key,
                      options?: AWS.DynamoDB.DocumentClient.PutItemInput) {
  return $put(assign({
    TableName: tableName,
    Item: item,
  }, options));
}

const $update = Observable.bindNodeCallback(
                (params: AWS.DynamoDB.DocumentClient.UpdateItemInput, cb: (err: any, data: any) => void) => 
                dynamodb.update(params, cb));
export function update(tableName: string, item: AWS.DynamoDB.DocumentClient.Key,
                        attrs: AWS.DynamoDB.DocumentClient.Key, options: AWS.DynamoDB.DocumentClient.UpdateItemInput) {
  const expressionSpec = buildExpression(attrs);

  const params: AWS.DynamoDB.DocumentClient.UpdateItemInput = assign({
    TableName: tableName,
    Key: item,
    UpdateExpression: `set ${expressionSpec.expression}`,
    ExpressionAttributeNames: expressionSpec.names,
    ExpressionAttributeValues: expressionSpec.values,
  }, options);

  return $update(params);
}

export interface IQueryOptions {
  indexName?: string;
  limit?: number;
  startKey?: AWS.DynamoDB.DocumentClient.Key;
  scanForward?: boolean;
};

const $query = Observable.bindNodeCallback(
                (params: AWS.DynamoDB.DocumentClient.QueryInput, cb: (err: any, data: any) => void) => 
                dynamodb.query(params, cb));
export function query(tableName: string,
                      expressions: AWS.DynamoDB.DocumentClient.Key | AWS.DynamoDB.DocumentClient.Key[],
                      options: IQueryOptions = {}, extraOptions: AWS.DynamoDB.DocumentClient.QueryInput) {
  const expressionSpec = buildExpression(expressions);

  const params: AWS.DynamoDB.DocumentClient.QueryInput = {
    TableName: tableName,
    KeyConditionExpression: expressionSpec.expression,
    ExpressionAttributeNames: expressionSpec.names,
    ExpressionAttributeValues: expressionSpec.values,
    ScanIndexForward: options.scanForward || false,
    IndexName: options.indexName,
  };

  if (options.limit) {
    params.Limit = options.limit;
  }

  if (options.startKey) {
    params.ExclusiveStartKey = options.startKey;
  }

  assign(params, extraOptions);

  return $query(params);
}

const $scan = Observable.bindNodeCallback(
                (params: AWS.DynamoDB.DocumentClient.ScanInput, cb: (err: any, data: any) => void) => 
                dynamodb.scan(params, cb));
export function scan(tableName: string,
                    expressions: AWS.DynamoDB.DocumentClient.Key | AWS.DynamoDB.DocumentClient.Key[],
                    options: IQueryOptions, extraOptions: AWS.DynamoDB.DocumentClient.ScanInput) {
  const expressionSpec = buildExpression(expressions);

  const params: AWS.DynamoDB.DocumentClient.ScanInput = {
    TableName: tableName,
    IndexName: options.indexName,
  };

  if (expressionSpec.expression) {
    params.FilterExpression = expressionSpec.expression;
    params.ExpressionAttributeNames = expressionSpec.names;
    params.ExpressionAttributeValues = expressionSpec.values;
  }

  if (options.indexName) {
    params.IndexName = options.indexName;
  }

  if (options.startKey) {
    params.ExclusiveStartKey = options.startKey;
  }

  if (options.limit) {
    params.Limit = options.limit;
  }

  assign(params, extraOptions);

  return $scan(params);
}

export class TableClass<T> {
  tableName = '';

  constructor(tableName: string) { this.tableName = tableName; }

  get (key: AWS.DynamoDB.DocumentClient.Key, options?: AWS.DynamoDB.DocumentClient.GetItemInput):
          Observable<{ Item: T }> {
    return get(this.tableName, key, options);
  }


  delete (key: AWS.DynamoDB.DocumentClient.Key, options?: AWS.DynamoDB.DocumentClient.DeleteItemInput):
        Observable<AWS.DynamoDB.DocumentClient.DeleteItemOutput> {
    return deleteItem(this.tableName, key, options);
  }

  put (item: T, options?: AWS.DynamoDB.DocumentClient.PutItemInput):
                        Observable<AWS.DynamoDB.DocumentClient.PutItemOutput> {
    return put(this.tableName, item, options);
  }

  update (item: AWS.DynamoDB.DocumentClient.Key, attrs: AWS.DynamoDB.DocumentClient.Key,
            options?: AWS.DynamoDB.DocumentClient.UpdateItemInput):
            Observable<AWS.DynamoDB.DocumentClient.UpdateItemOutput> {
    return update(this.tableName, item, attrs, options);
  }

  query (expression: AWS.DynamoDB.DocumentClient.Key | AWS.DynamoDB.DocumentClient.Key[], options: IQueryOptions,
          extraOptions?: AWS.DynamoDB.DocumentClient.QueryInput)
          :Observable<{ Items: T[] } & AWS.DynamoDB.DocumentClient.QueryOutput> {
    return query(this.tableName, expression, options, extraOptions);
  }

  scan (expression: AWS.DynamoDB.DocumentClient.Key | AWS.DynamoDB.DocumentClient.Key[], options: IQueryOptions,
          extraOptions?: AWS.DynamoDB.DocumentClient.ScanInput)
          :Observable<{ Items: T[] } & AWS.DynamoDB.DocumentClient.ScanOutput> {
    return scan(this.tableName, expression, options, extraOptions);
  }
}

export function table<T>(tableName: string) {
  return new TableClass(tableName);
}
