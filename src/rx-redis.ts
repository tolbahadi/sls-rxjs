import { Observable, Observer } from '@reactivex/rxjs';
import { logger } from './logger';
import { assign, each, map } from 'lodash';

import * as redis from 'redis';

export type redisCommand = 'get' | 'set' | 'setnx' | 'setex' | 'append' | 'strlen' | 'del' | 'exists' | 'setbit' |
                            'getbit' | 'setrange' |     'getrange' | 'substr' | 'incr' | 'decr' | 'mget' | 'rpush' |
                            'lpush' | 'rpushx' | 'lpushx' | 'linsert' | 'rpop' | 'lpop' |    'brpop' | 'brpoplpush' |
                            'blpop' | 'llen' | 'lindex' | 'lset' | 'lrange' | 'ltrim' | 'lrem' | 'rpoplpush' | 'sadd' |
                            'srem' |    'smove' | 'sismember' | 'scard' | 'spop' | 'srandmember' | 'sinter' |
                            'sinterstore' | 'sunion' | 'sunionstore' | 'sdiff' |    'sdiffstore' | 'smembers' |
                            'zadd' | 'zincrby' | 'zrem' | 'zremrangebyscore' | 'zremrangebyrank' | 'zunionstore' |
                            'zinterstore' | 'zrange' | 'zrangebyscore' | 'zrevrangebyscore' | 'zcount' | 'zrevrange' |
                            'zcard' | 'zscore' | 'zrank' |     'zrevrank' | 'hset' | 'hsetnx' | 'hget' | 'hmset' |
                            'hmget' | 'hincrby' | 'hincrbyfloat' | 'hdel' | 'hlen' | 'hkeys' |     'hvals' |
                            'hgetall' | 'hexists' | 'incrby' | 'decrby' | 'getset' | 'mset' | 'msetnx' | 'randomkey' |
                            'select' | 'move' |     'rename' | 'renamenx' | 'expire' | 'expireat' | 'keys' |
                            'dbsize' | 'auth' | 'ping' | 'echo' | 'save' | 'bgsave' |     'bgrewriteaof' |
                            'shutdown' | 'lastsave' | 'type' | 'multi' | 'exec' | 'discard' | 'sync' | 'flushdb' |
                            'flushall' | 'sort' | 'info' | 'monitor' | 'ttl' |'persist' | 'slaveof' | 'debug' |
                            'config' | 'subscribe' | 'unsubscribe' |     'psubscribe' | 'punsubscribe' | 'publish' |
                            'watch' | 'unwatch' | 'cluster' |     'restore' | 'migrate' | 'dump' | 'object' |
                            'client' | 'eval' | 'evalsha' | 'sscan';

export interface RedisMultiObservable extends redis.Multi {
  execObservable(): Observable<any>;
}

export class Redis {
  client: redis.RedisClient;
  log: Boolean = false;

  connect(log = false) {
    this.client = redis.createClient({
      host: process.env['REDIS'],
    });

    this.log = false;

    this.client.on('error', (error: Error) => {
      logger.error('RedisError', { error });
    });

    return this.client;
  }

  getClient() {
    return this.client;
  }

  quit() {
    this.client && this.client.quit();
    delete this.client;
  }

  method(name: redisCommand, ...args: any[]): Observable<any> {
    return Observable.create((observer: Observer<any>) => {
      const command = name + ' ' + (args || []).join(' ');
      if (this.log) {
        logger.info('Redis.Method', { command });
      }
      (<any>this.client)[name](...args, (error: Error, result: any) => {
        if (error) {
          logger.error('RedisMethodError', { command, error });
          observer.error(error);
        } else {
          observer.next(result);
          observer.complete();
        }
      });
    });
  }

  multi() {
    const multi = this.client.multi();

    const multiObservable = assign(multi, {
      execObservable: (): Observable<any> => {
        const commands = map((<any>multi).queue, (item: any) => item ? item.command + ' ' + item.args.join(' ') : null);
        if (this.log) {
          logger.info('Redis.ExecObservable', { commands });
        }
        return Observable.create((observer: Observer<any>) => {
          multi.exec((error: Error, result: any) => {
            if (error) {
              logger.error('ExecObservableFailed', { commands, error });
              observer.error(error);
            } else {
              observer.next(result);
              observer.complete();
            }
          });
        });
      },
    });
    
    return multiObservable;
  }
}

export function parseHash(hash: any) {
  if (!hash) {
    return;
  }

  const obj: any = {};

  each(hash, (value, key) => {
    try {
      obj[key] = JSON.parse(value);
    } catch (error) {
      logger.error('failed parsing value', { key, value, error });
    }
  });

  return obj;
}

export function serializeToHash(obj: any) {
  if (!obj) {
    return;
  }

  const hash: any = {};

  each(obj, (value, key) => {
    hash[key] = JSON.stringify(value);
  });

  return hash;
}
