// v1.1.2
import * as https from 'https';
import * as zlib from 'zlib';
import * as crypto from 'crypto';

type Request = {
  host: string,
  method: 'POST',
  path: string,
  body: string,
  headers: {
    [ key: string ]: string | number,
  },
};

const AWS_SECRET_ACCESS_KEY: string = process.env['AWS_SECRET_ACCESS_KEY'];
const AWS_ACCESS_KEY_ID: string = process.env['AWS_ACCESS_KEY_ID'];
const AWS_SESSION_TOKEN: string = process.env['AWS_SESSION_TOKEN'];

const endpoint = 'search-prod-sls-mauribac-us-east-1-4eclkp2fcnt4ks4xktmrt3ogem.us-east-1.es.amazonaws.com';

export function postToEs(path: string, body: string, callback: Function) {
    const requestParams = buildRequest(endpoint, path, body);

    const request = https.request(requestParams, function(response) {
        let responseBody = '';

        response.on('data', function(chunk: string) {
            responseBody += chunk;
        });

        response.on('end', function() {
            // const info = JSON.parse(responseBody);
            // const failedItems: any;
            // const success: any;
            
            // if (response.statusCode >= 200 && response.statusCode < 299) {
            //     failedItems = info.items.filter(function(x: any) {
            //         return x.index.status >= 300;
            //     });

            //     success = { 
            //         "attemptedItems": info.items.length,
            //         "successfulItems": info.items.length - failedItems.length,
            //         "failedItems": failedItems.length
            //     };
            // }

            // const error = response.statusCode !== 200 || info.errors === true ? {
            //     "statusCode": response.statusCode,
            //     "responseBody": responseBody
            // } : null;

            // callback(error, success, response.statusCode, failedItems);
            callback(null, responseBody);
        });
    }).on('error', function(e: Error) {
        callback(e);
    });

    request.end(requestParams.body);
}


function buildRequest(endpoint: string, path: string, body: string) {
    const endpointParts = endpoint.match(/^([^\.]+)\.?([^\.]*)\.?([^\.]*)\.amazonaws\.com$/);
    const region = endpointParts[2];
    const service = endpointParts[3];
    const datetime = (new Date()).toISOString().replace(/[:\-]|\.\d{3}/g, '');
    const date = datetime.substr(0, 8);
    const kDate = hmac('AWS4' + AWS_SECRET_ACCESS_KEY, date);
    const kRegion = hmac(kDate, region);
    const kService = hmac(kRegion, service);
    const kSigning = hmac(kService, 'aws4_request');
    
    const request: Request = {
        host: endpoint,
        method: 'POST',
        path,
        body,
        headers: {
          'Content-Type': 'application/json',
          Host: endpoint,
          'Content-Length': Buffer.byteLength(body),
          'X-Amz-Security-Token': AWS_SESSION_TOKEN,
          'X-Amz-Date': datetime,
        },
    };

    const canonicalHeaders = Object.keys(request.headers)
        .sort(function(a, b) { return a.toLowerCase() < b.toLowerCase() ? -1 : 1; })
        .map(function(k) { return k.toLowerCase() + ':' + request.headers[k]; })
        .join('\n');

    const signedHeaders = Object.keys(request.headers)
        .map(function(k) { return k.toLowerCase(); })
        .sort()
        .join(';');

    const canonicalString = [
        request.method,
        request.path, '',
        canonicalHeaders, '',
        signedHeaders,
        hash(request.body, 'hex'),
    ].join('\n');

    const credentialString = [ date, region, service, 'aws4_request' ].join('/');

    const stringToSign = [
        'AWS4-HMAC-SHA256',
        datetime,
        credentialString,
        hash(canonicalString, 'hex'),
    ] .join('\n');

    request.headers['Authorization'] = [
        'AWS4-HMAC-SHA256 Credential=' + AWS_ACCESS_KEY_ID + '/' + credentialString,
        'SignedHeaders=' + signedHeaders,
        'Signature=' + hmac(kSigning, stringToSign, 'hex'),
    ].join(', ');

    return request;
}

function hmac(key: string, str: string, encoding?: 'hex') {
    return crypto.createHmac('sha256', key).update(str, 'utf8').digest(encoding);
}

function hash(str: string, encoding: 'hex') {
    return crypto.createHash('sha256').update(str, 'utf8').digest(encoding);
}
