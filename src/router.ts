import { Observable, Subject } from '@reactivex/rxjs';
import { isString, isFunction } from 'lodash';
import * as pathToRegexp from 'path-to-regexp';

import { HttpRequest } from './request';
import { HttpResponse } from './response';

export interface ReqRes {
  req: HttpRequest;
  res: HttpResponse;
}

export interface RouteHandler {
  (arg: ReqRes): Observable<any>;
}

export class Router {
  subject: Observable<ReqRes>;
  constructor(subject?: Observable<ReqRes>) {
    if (!subject) {
      this.subject = new Subject<ReqRes>();
    } else {
      this.subject = subject;
    }
  }

  connect(source: Observable<ReqRes>) {
    const subject = <Subject<ReqRes>>this.subject;

    // source.subscribe(
    //   console.log.bind(console),
    //   console.log.bind(console)
    //   // console.log.bind(console),
    // );
    source.subscribe(
      subject.next.bind(subject),
      subject.error.bind(subject),
      subject.complete.bind(subject),
    );
  }

  subscribe() {
    this.subject.subscribe.apply(this.subject, arguments);
  }

  use(router: Router): Router;
  use(handler: RouteHandler): Router;
  use(path: string, handler: RouteHandler): Router;
  use(pathOrHandlerOrRouter: string | Router | RouteHandler, handler?: RouteHandler): Router {
    const path = isString(pathOrHandlerOrRouter) ? pathOrHandlerOrRouter : '';
    const router = pathOrHandlerOrRouter instanceof Router ? pathOrHandlerOrRouter : null;
    
    handler = isFunction(pathOrHandlerOrRouter) ? pathOrHandlerOrRouter : handler;

    let ob = this.subject;

    if (path) {
      let pathRegex = pathToRegexp(path);
      ob = this.subject.filter(({req, res}) => pathRegex.test(req.path));
    }

    if (handler) {
      ob = ob.flatMap(({req, res}) => {
        return handler({req, res}).mapTo({req, res});
      });
    } else {
      router.connect(ob);
      return router;
      // throw 'Not supported!';
      // ob.share().subscribe(router.subject);
      // ob = ob.flatMap(router.subject);
    }

    return new Router(ob);
  }

  method(method: string, path: string, handler: RouteHandler) {
    return new Router(
      this.subject.filter(({ req, res}) => {
        return req.httpMethod.toLocaleLowerCase() === method && pathToRegexp(path).test(req.path);
      })
      .flatMap((arg) => handler(arg).mapTo(arg)));
  }

  get(path: string, handler: RouteHandler) {
    return this.method('get', path, handler);
  }

  post(path: string, handler: RouteHandler) {
    return this.method('post', path, handler);
  }

  put(path: string, handler: RouteHandler) {
    return this.method('put', path, handler);
  }

  delete(path: string, handler: RouteHandler) {
    return this.method('delete', path, handler);
  }

  option(path: string, handler: RouteHandler) {
    return this.method('option', path, handler);
  }

  head(path: string, handler: RouteHandler) {
    return this.method('head', path, handler);
  }
}

