import { Observable, Observer, Subject } from '@reactivex/rxjs';
import { Response, HttpResponse } from './response';
import { HttpRequest } from './request';
import { Router } from './router';
import { LambdaCallback } from './aws-lambda';

export interface ILambdaInvokation {
  event: any;
  context: any;
  callback: LambdaCallback;
}

export interface ILambdaHttpInvokation {
  context: any;
  req: HttpRequest;
  res: HttpResponse;
}

export type lambdaHandler = (event: any, context: any, callback: LambdaCallback) => void;

export function getHandler(): { lambdaFn: lambdaHandler, $subject: Observable<ILambdaInvokation> } {
  const $subject = new Subject<ILambdaInvokation>();
  
  const lambdaFn = (event: any, context: any, callback: LambdaCallback) => {
    $subject.next({ event, context, callback });
  };

  return { lambdaFn, $subject };
}

export function getHttpHandler():
{ lambdaFn: lambdaHandler, $subject: Observable<ILambdaHttpInvokation>, app: Router } {
  const { lambdaFn, $subject } = getHandler();

  const $httpSubject = $subject.map((invokation) => {
    return {
      context: invokation.context,
      req: new HttpRequest(invokation.event),
      res: new HttpResponse(invokation.callback),
    };
  });

  const app = new Router($httpSubject);

  return { lambdaFn, $subject: $httpSubject, app };
}
