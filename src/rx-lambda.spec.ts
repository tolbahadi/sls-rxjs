import { getHandler } from './rx-lambda';
declare function describe(name: string, f: Function): void;
declare function it(name: string, f: (done?: Function) => void): void;
declare function expect(value: any): {toEqual: (val: any) => void};

describe('rx-lambda', () => {
  describe('getHandler', () => {

    it('returns observer', (done) => {
      const { lambdaFn, $subject } = getHandler();

      const event = {};
      const context = {};
      const callback = () => { return; };

      $subject.subscribe((value: any) => { 
        expect(value).toEqual({
          event,
          context,
          callback,
        });
        done();
      });

      lambdaFn(event, context, callback);
    });
  });
});
