import { assign, each, pick } from 'lodash';
import { postToEs } from './post-to-es';

export interface Meta {
  [ key: string ]: any;
}

export type Levels = 'trace' | 'debug' | 'log' | 'info' | 'error';

export enum LogLevel {
  error,
  info,
  log,
  debug,
  trace,
}

const logLevelToStr = {
  [ LogLevel.trace ]: 'trace',
  [ LogLevel.debug ]: 'debug',
  [ LogLevel.log ]: 'log',
  [ LogLevel.info ]: 'info',
  [ LogLevel.error ]: 'error',
};

const strToLogLevel: { [key: string ]: LogLevel } = {
  trace: LogLevel.trace,
  debug: LogLevel.debug,
  log: LogLevel.log,
  info: LogLevel.info,
  error: LogLevel.error,
};

export function serialize(level: LogLevel, message: string, meta?: Meta): LogObj {
  const obj = <LogObj>{};

  each(meta, (value, key) => {
    if (key === 'error') {
      value = pick(value, [ 'message', 'stack' ]);
    }
    obj[key] = value;
  });

  assign(obj, {
    timestamp: new Date(),
    level: logLevelToStr[level],
    message,
  });

  return obj;
}

export interface LogObj {
  level: Levels;
  message: string;
  timestamp: Date;
  [ key: string ]: any;
}

export interface Transport {
  level: Levels;
  log (level: LogLevel, message: string, meta?: Meta): void;
}

export function shouldTransportLog(transportLevel: string, logLevel: LogLevel) {
  return strToLogLevel[transportLevel] >= logLevel;
}

export class ConsoleTransport implements Transport {
  level: Levels;
  json: boolean;
  constructor({ level: lvl = 'log', json: isJson = true }: { level?: Levels, json?: boolean }) {
    this.level = lvl;
    this.json = isJson;
  }

  log(level: LogLevel, message: string, meta?: Meta) {
    if (shouldTransportLog(this.level, level)) {
      if (this.json) {
        console.log(JSON.stringify(serialize(level, message, meta)));
      } else {
        console.log(logLevelToStr[level], ':', message, meta);
      }
    }
  }
}

const functionName = process.env['AWS_LAMBDA_FUNCTION_NAME'];

export class EsTransport implements Transport {
  level: Levels;
  index: string;
  type: string;

  constructor({ level, index, type }: { level?: Levels, index?: string, type?: string }) {
    this.level = level || 'log';
    this.index = index || 'log';
    this.type = type || functionName;
  }

  log(level: LogLevel, message: string, meta?: Meta) {
    if (shouldTransportLog(this.level, level)) {
      const logObj = serialize(level, message, meta);

      let indexName = [
        this.index + '-' + logObj.timestamp.getUTCFullYear(),              // year
        ('0' + (logObj.timestamp.getUTCMonth() + 1)).slice(-2),  // month
        ('0' + logObj.timestamp.getUTCDate()).slice(-2),          // day
      ].join('.');

      const logType = this.type;

      postToEs(`/${indexName}/${logType}`, JSON.stringify(logObj), (err: Error, info: any) => {
        if (err) {
          console.log('PostToEs failed:', err);
        } else {
          console.log('PostToEs success', info);
        }
      });
    }
  }
}

export const transports = {
  ConsoleTransport,
};

export class Logger {
  transports: Transport[];

  constructor({ transports: trs = [ new ConsoleTransport({}) ] }: { transports: Transport[] }) {
    this.transports = trs;
  }

  private processLog(level: LogLevel, message: string, meta?: Meta) {
    this.transports.forEach((transport) => transport.log(level, message, meta));
  }

  trace(message: string, meta?: Meta) {
    this.processLog(LogLevel.trace, message, meta);
  }

  debug(message: string, meta?: Meta) {
    this.processLog(LogLevel.debug, message, meta);
  }

  log(message: string, meta?: Meta) {
    this.processLog(LogLevel.log, message, meta);
  }

  info(message: string, meta?: Meta) {
    this.processLog(LogLevel.info, message, meta);
  }

  error(message: string, meta?: Meta) {
    this.processLog(LogLevel.error, message, meta);
  }

  private cachedTimeStamps: { [key: string]: number } = {};

  profile(id: string, message?: string, meta?: Meta) {
    if (this.cachedTimeStamps[id]) {
      let newMeta = assign({}, meta, { duration: Date.now() - this.cachedTimeStamps[id], isProfile: true });
      this.info(message, newMeta);
      delete this.cachedTimeStamps[id];
    } else {
      this.cachedTimeStamps[id] = Date.now();
    }
  }
}

const loggerTransports: Transport[] = [ new ConsoleTransport({ json: true }) ];

// if (!process.env['IS_OFFLINE']) {
//   loggerTransports.push(new EsTransport({}));
// }

export const logger = new Logger({
  transports: loggerTransports,
});
