import { SQS } from 'aws-sdk';
import { Observable, Observer } from '@reactivex/rxjs';

const sqs = new SQS({
  apiVersion: '2012-11-05',
  region: 'us-east-1',
});

export class RxSQS<T> {
  queuUrl: string;

  sendMessage(data: T, delay: number = 0) {
    return Observable.create((observer: Observer<SQS.SendMessageResult>) => {
      sqs.sendMessage({
        DelaySeconds: delay,
        MessageBody: JSON.stringify(data),
        QueueUrl: this.queuUrl,
      }, (error: Error, result) => {
        if (error) {
          observer.error(error);
        } else {
          observer.next(result);
          observer.complete();
        }
      });
    });
  }

  receiveMessage(max = 1): Observable<{ data: T, message: SQS.Message }> {
    return Observable.create((observer: Observer<{ data: T, message: SQS.Message }>) => {
      sqs.receiveMessage({
        QueueUrl: this.queuUrl,
        MaxNumberOfMessages: max,
      }, (error: Error, result) => {
        if (error) {
          observer.error(error);
        } else {
          if (result.Messages) {
            result.Messages.forEach((message) => {
              observer.next({
                data: <T>JSON.parse(message.Body),
                message,
              });
            });
          }
          observer.complete();
        }
      });
    });
  }

  deleteMessage(receiptHandler: string) {
    return Observable.create((observer: Observer<{}>) => {
      sqs.deleteMessage({
        QueueUrl: this.queuUrl,
        ReceiptHandle: receiptHandler,
      }, (error: Error, result) => {
        if (error) {
          observer.error(error);
        } else {
          observer.next(result);
          observer.complete();
        }
      });
    });
  }
}
