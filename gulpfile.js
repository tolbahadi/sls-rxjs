'use strict';

const gulp = require('gulp');
const ts = require('gulp-typescript');
const gulpSequence = require('gulp-sequence');
const jasmine = require('gulp-jasmine');

const config = {
  src: ['src/**/*.ts'],
  dest: 'lib',
};

// linting
const tslint = require('gulp-tslint');

gulp.task('lint', () =>
  gulp.src(config.src)
    .pipe(tslint({
      formatter: 'verbose'
    }))
    .pipe(tslint.report())
);

// typescript
const tsProject = ts.createProject('tsconfig.json');

gulp.task('scripts', () => {
  return gulp.src(config.src)
    .pipe(tsProject())
    .pipe(gulp.dest(config.dest));
});

gulp.task('test', () => {
  return gulp.src('lib/**/*.spec.js')
    .pipe(jasmine());
});

gulp.task('build', gulpSequence('lint', 'scripts', 'test')); // TODO: replace with gulp.series

gulp.task('watch', () => {
  gulp.watch(config.src, ['build']);
});

gulp.task('default', ['build']);
