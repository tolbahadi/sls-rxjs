import { SQS } from 'aws-sdk';
import { Observable } from '@reactivex/rxjs';
export declare class RxSQS<T> {
    queuUrl: string;
    sendMessage(data: T, delay?: number): any;
    receiveMessage(max?: number): Observable<{
        data: T;
        message: SQS.Message;
    }>;
    deleteMessage(receiptHandler: string): any;
}
