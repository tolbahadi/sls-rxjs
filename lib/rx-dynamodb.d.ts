import * as AWS from 'aws-sdk';
import { Observable } from '@reactivex/rxjs';
export declare function get(tableName: string, key: AWS.DynamoDB.DocumentClient.Key, options: AWS.DynamoDB.DocumentClient.GetItemInput): Observable<any>;
export declare function deleteItem(tableName: string, key: AWS.DynamoDB.DocumentClient.Key, options: AWS.DynamoDB.DocumentClient.DeleteItemInput): Observable<any>;
export declare function put(tableName: string, item: AWS.DynamoDB.DocumentClient.Key, options?: AWS.DynamoDB.DocumentClient.PutItemInput): Observable<any>;
export declare function update(tableName: string, item: AWS.DynamoDB.DocumentClient.Key, attrs: AWS.DynamoDB.DocumentClient.Key, options: AWS.DynamoDB.DocumentClient.UpdateItemInput): Observable<any>;
export interface IQueryOptions {
    indexName?: string;
    limit?: number;
    startKey?: AWS.DynamoDB.DocumentClient.Key;
    scanForward?: boolean;
}
export declare function query(tableName: string, expressions: AWS.DynamoDB.DocumentClient.Key | AWS.DynamoDB.DocumentClient.Key[], options: IQueryOptions, extraOptions: AWS.DynamoDB.DocumentClient.QueryInput): Observable<any>;
export declare function scan(tableName: string, expressions: AWS.DynamoDB.DocumentClient.Key | AWS.DynamoDB.DocumentClient.Key[], options: IQueryOptions, extraOptions: AWS.DynamoDB.DocumentClient.ScanInput): Observable<any>;
export declare class TableClass<T> {
    tableName: string;
    constructor(tableName: string);
    get(key: AWS.DynamoDB.DocumentClient.Key, options?: AWS.DynamoDB.DocumentClient.GetItemInput): Observable<{
        Item: T;
    }>;
    delete(key: AWS.DynamoDB.DocumentClient.Key, options?: AWS.DynamoDB.DocumentClient.DeleteItemInput): Observable<AWS.DynamoDB.DocumentClient.DeleteItemOutput>;
    put(item: T, options?: AWS.DynamoDB.DocumentClient.PutItemInput): Observable<AWS.DynamoDB.DocumentClient.PutItemOutput>;
    update(item: AWS.DynamoDB.DocumentClient.Key, attrs: AWS.DynamoDB.DocumentClient.Key, options?: AWS.DynamoDB.DocumentClient.UpdateItemInput): Observable<AWS.DynamoDB.DocumentClient.UpdateItemOutput>;
    query(expression: AWS.DynamoDB.DocumentClient.Key | AWS.DynamoDB.DocumentClient.Key[], options: IQueryOptions, extraOptions?: AWS.DynamoDB.DocumentClient.QueryInput): Observable<{
        Items: T[];
    } & AWS.DynamoDB.DocumentClient.QueryOutput>;
    scan(expression: AWS.DynamoDB.DocumentClient.Key | AWS.DynamoDB.DocumentClient.Key[], options: IQueryOptions, extraOptions?: AWS.DynamoDB.DocumentClient.ScanInput): Observable<{
        Items: T[];
    } & AWS.DynamoDB.DocumentClient.ScanOutput>;
}
export declare function table<T>(tableName: string): TableClass<{}>;
