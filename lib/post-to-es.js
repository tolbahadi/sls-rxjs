"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// v1.1.2
var https = require("https");
var crypto = require("crypto");
var AWS_SECRET_ACCESS_KEY = process.env['AWS_SECRET_ACCESS_KEY'];
var AWS_ACCESS_KEY_ID = process.env['AWS_ACCESS_KEY_ID'];
var AWS_SESSION_TOKEN = process.env['AWS_SESSION_TOKEN'];
var endpoint = 'search-prod-sls-mauribac-us-east-1-4eclkp2fcnt4ks4xktmrt3ogem.us-east-1.es.amazonaws.com';
function postToEs(path, body, callback) {
    var requestParams = buildRequest(endpoint, path, body);
    var request = https.request(requestParams, function (response) {
        var responseBody = '';
        response.on('data', function (chunk) {
            responseBody += chunk;
        });
        response.on('end', function () {
            // const info = JSON.parse(responseBody);
            // const failedItems: any;
            // const success: any;
            // if (response.statusCode >= 200 && response.statusCode < 299) {
            //     failedItems = info.items.filter(function(x: any) {
            //         return x.index.status >= 300;
            //     });
            //     success = { 
            //         "attemptedItems": info.items.length,
            //         "successfulItems": info.items.length - failedItems.length,
            //         "failedItems": failedItems.length
            //     };
            // }
            // const error = response.statusCode !== 200 || info.errors === true ? {
            //     "statusCode": response.statusCode,
            //     "responseBody": responseBody
            // } : null;
            // callback(error, success, response.statusCode, failedItems);
            callback(null, responseBody);
        });
    }).on('error', function (e) {
        callback(e);
    });
    request.end(requestParams.body);
}
exports.postToEs = postToEs;
function buildRequest(endpoint, path, body) {
    var endpointParts = endpoint.match(/^([^\.]+)\.?([^\.]*)\.?([^\.]*)\.amazonaws\.com$/);
    var region = endpointParts[2];
    var service = endpointParts[3];
    var datetime = (new Date()).toISOString().replace(/[:\-]|\.\d{3}/g, '');
    var date = datetime.substr(0, 8);
    var kDate = hmac('AWS4' + AWS_SECRET_ACCESS_KEY, date);
    var kRegion = hmac(kDate, region);
    var kService = hmac(kRegion, service);
    var kSigning = hmac(kService, 'aws4_request');
    var request = {
        host: endpoint,
        method: 'POST',
        path: path,
        body: body,
        headers: {
            'Content-Type': 'application/json',
            Host: endpoint,
            'Content-Length': Buffer.byteLength(body),
            'X-Amz-Security-Token': AWS_SESSION_TOKEN,
            'X-Amz-Date': datetime,
        },
    };
    var canonicalHeaders = Object.keys(request.headers)
        .sort(function (a, b) { return a.toLowerCase() < b.toLowerCase() ? -1 : 1; })
        .map(function (k) { return k.toLowerCase() + ':' + request.headers[k]; })
        .join('\n');
    var signedHeaders = Object.keys(request.headers)
        .map(function (k) { return k.toLowerCase(); })
        .sort()
        .join(';');
    var canonicalString = [
        request.method,
        request.path, '',
        canonicalHeaders, '',
        signedHeaders,
        hash(request.body, 'hex'),
    ].join('\n');
    var credentialString = [date, region, service, 'aws4_request'].join('/');
    var stringToSign = [
        'AWS4-HMAC-SHA256',
        datetime,
        credentialString,
        hash(canonicalString, 'hex'),
    ].join('\n');
    request.headers['Authorization'] = [
        'AWS4-HMAC-SHA256 Credential=' + AWS_ACCESS_KEY_ID + '/' + credentialString,
        'SignedHeaders=' + signedHeaders,
        'Signature=' + hmac(kSigning, stringToSign, 'hex'),
    ].join(', ');
    return request;
}
function hmac(key, str, encoding) {
    return crypto.createHmac('sha256', key).update(str, 'utf8').digest(encoding);
}
function hash(str, encoding) {
    return crypto.createHash('sha256').update(str, 'utf8').digest(encoding);
}
