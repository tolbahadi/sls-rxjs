import { Observable } from '@reactivex/rxjs';
import { HttpResponse } from './response';
import { HttpRequest } from './request';
import { Router } from './router';
import { LambdaCallback } from './aws-lambda';
export interface ILambdaInvokation {
    event: any;
    context: any;
    callback: LambdaCallback;
}
export interface ILambdaHttpInvokation {
    context: any;
    req: HttpRequest;
    res: HttpResponse;
}
export declare type lambdaHandler = (event: any, context: any, callback: LambdaCallback) => void;
export declare function getHandler(): {
    lambdaFn: lambdaHandler;
    $subject: Observable<ILambdaInvokation>;
};
export declare function getHttpHandler(): {
    lambdaFn: lambdaHandler;
    $subject: Observable<ILambdaHttpInvokation>;
    app: Router;
};
