"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Events = require("events");
var cookie = require("cookie");
var _ = require("lodash");
var Response = (function (_super) {
    __extends(Response, _super);
    function Response(callback) {
        var _this = _super.call(this) || this;
        _this.lambdaCallbak = callback;
        _this.finished = false;
        return _this;
    }
    Response.prototype.send = function (data) {
        if (!this.finished) {
            this.lambdaCallbak(null, data);
            this.finished = true;
            this.emit('finish');
        }
        else {
            var error = new Error('Response has been sent already!');
            // this.emit('error', error);
            throw error;
        }
    };
    Response.prototype.fail = function (error) {
        this.lambdaCallbak(error);
    };
    return Response;
}(Events));
exports.Response = Response;
var getNoCacheHeader = function () { return ({
    'Cache-Control': 'private, max-age=0, no-cache, no-store, must-revalidate',
    Pragma: 'no-cache',
    Expires: '0',
}); };
var HttpResponse = (function (_super) {
    __extends(HttpResponse, _super);
    function HttpResponse() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.headers = {};
        _this.cookies = {};
        _this.statusCode = 200;
        _this.noCache = true;
        return _this;
    }
    HttpResponse.prototype.set = function (name, value) {
        this.headers[name] = value;
        return this;
    };
    HttpResponse.prototype.setCookie = function (name, value) {
        this.cookies[name] = value;
    };
    HttpResponse.prototype.status = function (value) {
        this.statusCode = value;
        return this;
    };
    HttpResponse.prototype.getStatus = function () {
        return this.statusCode;
    };
    HttpResponse.prototype.send = function (data, isBase64Encoded) {
        if (isBase64Encoded === void 0) { isBase64Encoded = false; }
        var headers = this.headers;
        if (this.noCache) {
            headers = __assign({}, headers, getNoCacheHeader());
        }
        var response = {
            statusCode: this.statusCode,
            headers: headers,
            body: '',
            isBase64Encoded: isBase64Encoded,
        };
        if (!_.isEmpty(this.cookies)) {
            response.headers['Set-Cookie'] = _.map(this.cookies, function (val, key) { return cookie.serialize(key, val); }).join('; ');
        }
        if (typeof data === 'string') {
            response.headers['Content-Type'] = response.headers['Content-Type'] || 'text/html';
            response.body = data;
        }
        else {
            response.headers['Content-Type'] = response.headers['Content-Type'] || 'application/json';
            response.body = JSON.stringify(data);
        }
        _super.prototype.send.call(this, response);
    };
    return HttpResponse;
}(Response));
exports.HttpResponse = HttpResponse;
