import { Observable } from '@reactivex/rxjs';
import { HttpRequest } from './request';
import { HttpResponse } from './response';
export interface ReqRes {
    req: HttpRequest;
    res: HttpResponse;
}
export interface RouteHandler {
    (arg: ReqRes): Observable<any>;
}
export declare class Router {
    subject: Observable<ReqRes>;
    constructor(subject?: Observable<ReqRes>);
    connect(source: Observable<ReqRes>): void;
    subscribe(): void;
    use(router: Router): Router;
    use(handler: RouteHandler): Router;
    use(path: string, handler: RouteHandler): Router;
    method(method: string, path: string, handler: RouteHandler): Router;
    get(path: string, handler: RouteHandler): Router;
    post(path: string, handler: RouteHandler): Router;
    put(path: string, handler: RouteHandler): Router;
    delete(path: string, handler: RouteHandler): Router;
    option(path: string, handler: RouteHandler): Router;
    head(path: string, handler: RouteHandler): Router;
}
