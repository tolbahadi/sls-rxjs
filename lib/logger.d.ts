export interface Meta {
    [key: string]: any;
}
export declare type Levels = 'trace' | 'debug' | 'log' | 'info' | 'error';
export declare enum LogLevel {
    error = 0,
    info = 1,
    log = 2,
    debug = 3,
    trace = 4,
}
export declare function serialize(level: LogLevel, message: string, meta?: Meta): LogObj;
export interface LogObj {
    level: Levels;
    message: string;
    timestamp: Date;
    [key: string]: any;
}
export interface Transport {
    level: Levels;
    log(level: LogLevel, message: string, meta?: Meta): void;
}
export declare function shouldTransportLog(transportLevel: string, logLevel: LogLevel): boolean;
export declare class ConsoleTransport implements Transport {
    level: Levels;
    json: boolean;
    constructor({level: lvl, json: isJson}: {
        level?: Levels;
        json?: boolean;
    });
    log(level: LogLevel, message: string, meta?: Meta): void;
}
export declare class EsTransport implements Transport {
    level: Levels;
    index: string;
    type: string;
    constructor({level, index, type}: {
        level?: Levels;
        index?: string;
        type?: string;
    });
    log(level: LogLevel, message: string, meta?: Meta): void;
}
export declare const transports: {
    ConsoleTransport: typeof ConsoleTransport;
};
export declare class Logger {
    transports: Transport[];
    constructor({transports: trs}: {
        transports: Transport[];
    });
    private processLog(level, message, meta?);
    trace(message: string, meta?: Meta): void;
    debug(message: string, meta?: Meta): void;
    log(message: string, meta?: Meta): void;
    info(message: string, meta?: Meta): void;
    error(message: string, meta?: Meta): void;
    private cachedTimeStamps;
    profile(id: string, message?: string, meta?: Meta): void;
}
export declare const logger: Logger;
