"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var lodash_1 = require("lodash");
var post_to_es_1 = require("./post-to-es");
var LogLevel;
(function (LogLevel) {
    LogLevel[LogLevel["error"] = 0] = "error";
    LogLevel[LogLevel["info"] = 1] = "info";
    LogLevel[LogLevel["log"] = 2] = "log";
    LogLevel[LogLevel["debug"] = 3] = "debug";
    LogLevel[LogLevel["trace"] = 4] = "trace";
})(LogLevel = exports.LogLevel || (exports.LogLevel = {}));
var logLevelToStr = (_a = {},
    _a[LogLevel.trace] = 'trace',
    _a[LogLevel.debug] = 'debug',
    _a[LogLevel.log] = 'log',
    _a[LogLevel.info] = 'info',
    _a[LogLevel.error] = 'error',
    _a);
var strToLogLevel = {
    trace: LogLevel.trace,
    debug: LogLevel.debug,
    log: LogLevel.log,
    info: LogLevel.info,
    error: LogLevel.error,
};
function serialize(level, message, meta) {
    var obj = {};
    lodash_1.each(meta, function (value, key) {
        if (key === 'error') {
            value = lodash_1.pick(value, ['message', 'stack']);
        }
        obj[key] = value;
    });
    lodash_1.assign(obj, {
        timestamp: new Date(),
        level: logLevelToStr[level],
        message: message,
    });
    return obj;
}
exports.serialize = serialize;
function shouldTransportLog(transportLevel, logLevel) {
    return strToLogLevel[transportLevel] >= logLevel;
}
exports.shouldTransportLog = shouldTransportLog;
var ConsoleTransport = (function () {
    function ConsoleTransport(_a) {
        var _b = _a.level, lvl = _b === void 0 ? 'log' : _b, _c = _a.json, isJson = _c === void 0 ? true : _c;
        this.level = lvl;
        this.json = isJson;
    }
    ConsoleTransport.prototype.log = function (level, message, meta) {
        if (shouldTransportLog(this.level, level)) {
            if (this.json) {
                console.log(JSON.stringify(serialize(level, message, meta)));
            }
            else {
                console.log(logLevelToStr[level], ':', message, meta);
            }
        }
    };
    return ConsoleTransport;
}());
exports.ConsoleTransport = ConsoleTransport;
var functionName = process.env['AWS_LAMBDA_FUNCTION_NAME'];
var EsTransport = (function () {
    function EsTransport(_a) {
        var level = _a.level, index = _a.index, type = _a.type;
        this.level = level || 'log';
        this.index = index || 'log';
        this.type = type || functionName;
    }
    EsTransport.prototype.log = function (level, message, meta) {
        if (shouldTransportLog(this.level, level)) {
            var logObj = serialize(level, message, meta);
            var indexName = [
                this.index + '-' + logObj.timestamp.getUTCFullYear(),
                ('0' + (logObj.timestamp.getUTCMonth() + 1)).slice(-2),
                ('0' + logObj.timestamp.getUTCDate()).slice(-2),
            ].join('.');
            var logType = this.type;
            post_to_es_1.postToEs("/" + indexName + "/" + logType, JSON.stringify(logObj), function (err, info) {
                if (err) {
                    console.log('PostToEs failed:', err);
                }
                else {
                    console.log('PostToEs success', info);
                }
            });
        }
    };
    return EsTransport;
}());
exports.EsTransport = EsTransport;
exports.transports = {
    ConsoleTransport: ConsoleTransport,
};
var Logger = (function () {
    function Logger(_a) {
        var _b = _a.transports, trs = _b === void 0 ? [new ConsoleTransport({})] : _b;
        this.cachedTimeStamps = {};
        this.transports = trs;
    }
    Logger.prototype.processLog = function (level, message, meta) {
        this.transports.forEach(function (transport) { return transport.log(level, message, meta); });
    };
    Logger.prototype.trace = function (message, meta) {
        this.processLog(LogLevel.trace, message, meta);
    };
    Logger.prototype.debug = function (message, meta) {
        this.processLog(LogLevel.debug, message, meta);
    };
    Logger.prototype.log = function (message, meta) {
        this.processLog(LogLevel.log, message, meta);
    };
    Logger.prototype.info = function (message, meta) {
        this.processLog(LogLevel.info, message, meta);
    };
    Logger.prototype.error = function (message, meta) {
        this.processLog(LogLevel.error, message, meta);
    };
    Logger.prototype.profile = function (id, message, meta) {
        if (this.cachedTimeStamps[id]) {
            var newMeta = lodash_1.assign({}, meta, { duration: Date.now() - this.cachedTimeStamps[id], isProfile: true });
            this.info(message, newMeta);
            delete this.cachedTimeStamps[id];
        }
        else {
            this.cachedTimeStamps[id] = Date.now();
        }
    };
    return Logger;
}());
exports.Logger = Logger;
var loggerTransports = [new ConsoleTransport({ json: true })];
// if (!process.env['IS_OFFLINE']) {
//   loggerTransports.push(new EsTransport({}));
// }
exports.logger = new Logger({
    transports: loggerTransports,
});
var _a;
