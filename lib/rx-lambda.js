"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var rxjs_1 = require("@reactivex/rxjs");
var response_1 = require("./response");
var request_1 = require("./request");
var router_1 = require("./router");
function getHandler() {
    var $subject = new rxjs_1.Subject();
    var lambdaFn = function (event, context, callback) {
        $subject.next({ event: event, context: context, callback: callback });
    };
    return { lambdaFn: lambdaFn, $subject: $subject };
}
exports.getHandler = getHandler;
function getHttpHandler() {
    var _a = getHandler(), lambdaFn = _a.lambdaFn, $subject = _a.$subject;
    var $httpSubject = $subject.map(function (invokation) {
        return {
            context: invokation.context,
            req: new request_1.HttpRequest(invokation.event),
            res: new response_1.HttpResponse(invokation.callback),
        };
    });
    var app = new router_1.Router($httpSubject);
    return { lambdaFn: lambdaFn, $subject: $httpSubject, app: app };
}
exports.getHttpHandler = getHttpHandler;
