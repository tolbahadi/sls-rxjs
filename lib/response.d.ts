/// <reference types="node" />
import * as Events from 'events';
import { LambdaCallback } from './aws-lambda';
export declare class Response extends Events {
    private lambdaCallbak;
    finished: boolean;
    constructor(callback: LambdaCallback);
    send(data: any): void;
    fail(error: Error): void;
}
export declare class HttpResponse extends Response {
    private headers;
    private cookies;
    private statusCode;
    noCache: boolean;
    set(name: string, value: string): this;
    setCookie(name: string, value: string): void;
    status(value: number): this;
    getStatus(): number;
    send(data: any, isBase64Encoded?: boolean): void;
}
