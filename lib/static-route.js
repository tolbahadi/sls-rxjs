"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var path = require("path");
var url = require("url");
var rxjs_1 = require("@reactivex/rxjs");
var mime = require("mime");
function staticRouteHandler(root, shouldCache) {
    return function (req, res) {
        var fullPath = path.join(root, url.parse(req.path).path.replace(/^\//, ''));
        var type = mime.lookup(fullPath);
        fs.readFile(fullPath, 'utf8', function (error, content) {
            if (error) {
                res.status(500).send({ message: error.message });
            }
            else {
                res.set('Content-Type', type);
                if (shouldCache(fullPath)) {
                    res.set('Cache-Control', 'public, max-age=31104000');
                    res.set('Expires', new Date(Date.now() + 1000 * 60 * 60 * 24 * 30 * 12).toISOString());
                }
                res.send(content);
            }
        });
    };
    function getFileContent(filePath) {
        return rxjs_1.Observable.create(function (observer) {
            fs.readFile(path.join(root, filePath), 'utf8', function (error, data) {
                if (error) {
                    observer.error(error);
                }
                else {
                    observer.next(data);
                    observer.complete();
                }
            });
        });
    }
}
exports.staticRouteHandler = staticRouteHandler;
