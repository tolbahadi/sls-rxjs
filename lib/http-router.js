"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var lodash_1 = require("lodash");
var rxjs_1 = require("@reactivex/rxjs");
var pathToRegexp = require("path-to-regexp");
var Router = (function () {
    function Router() {
        this.handlers = [];
        this.mountPrefix = '';
    }
    // addHandler(method: Method, handler: RouteHandler)
    Router.prototype.addHandler = function (method, path, handler) {
        if (!lodash_1.isString(path)) {
            handler = path;
            path = '*';
        }
        if (handler instanceof Router) {
            path = path === '*' ? '' : path;
            handler.mountPrefix = this.mountPrefix + path;
        }
        var keys = [];
        var regex = pathToRegexp(path, keys);
        this.handlers.push({
            method: method,
            path: path,
            regex: regex,
            keys: keys,
            handler: handler,
        });
        return this;
    };
    Router.prototype.error = function (error, req, res) {
        if (this.errorHandler != null) {
            this.errorHandler(error, req, res);
        }
        else {
            throw new Error('Error handler not registered.');
        }
    };
    Router.prototype.get = function (path, handler) {
        return this.addHandler('GET', path, handler);
    };
    Router.prototype.put = function (path, handler) {
        return this.addHandler('PUT', path, handler);
    };
    Router.prototype.post = function (path, handler) {
        return this.addHandler('POST', path, handler);
    };
    Router.prototype.delete = function (path, handler) {
        return this.addHandler('DELETE', path, handler);
    };
    Router.prototype.all = function (path, handler) {
        return this.addHandler('ANY', path, handler);
    };
    Router.prototype.use = function (path, handler) {
        return this.addHandler('ANY', path, handler);
    };
    // param(path: string, handler: RouteHandler) {
    //   return this.addHandler('ANY', path, handler);
    // }
    Router.prototype._shouldCallMiddleware = function (req, middleware) {
        if (middleware.handler instanceof Router) {
            return req.path.replace(this.mountPrefix, '').indexOf(middleware.path) === 0 &&
                (middleware.method === req.httpMethod || middleware.method === 'ANY');
        }
        return middleware.regex.test(req.path.replace(this.mountPrefix, '')) &&
            (middleware.method === req.httpMethod || middleware.method === 'ANY');
    };
    Router.prototype._callMiddleware = function (req, res, middleware, next) {
        if (middleware.handler instanceof Router) {
            middleware.handler._processMiddleware(req, res, next);
            return;
        }
        var parts = middleware.regex.exec(req.path.replace(this.mountPrefix, ''));
        var params = {};
        if (parts) {
            for (var i = 1; i < parts.length; i++) {
                var key = middleware.keys[i - 1];
                var name_1 = key.name;
                var value = decodeParam(parts[i]);
                if (value !== undefined && !Object.prototype.hasOwnProperty.call(params, name_1)) {
                    params[name_1] = value;
                }
            }
        }
        if (!lodash_1.isEmpty(params)) {
            req.params = lodash_1.assign({}, req.params, params);
        }
        middleware.handler(req, res, next);
    };
    Router.prototype._processMiddleware = function (req, res, next) {
        var _this = this;
        return rxjs_1.Observable.create(function (observer) {
            var index = -1;
            var triggerNext = function () {
                index += 1;
                if (index >= _this.handlers.length) {
                    observer.complete();
                    return;
                }
                var middleware = _this.handlers[index];
                if (_this._shouldCallMiddleware(req, middleware)) {
                    _this._callMiddleware(req, res, middleware, function (error) {
                        if (error) {
                            observer.error(error);
                        }
                        else {
                            triggerNext();
                        }
                    });
                }
                else {
                    triggerNext();
                }
            };
            triggerNext();
        })
            .subscribe(null, next, function () { return next(); });
    };
    return Router;
}());
exports.Router = Router;
var App = (function (_super) {
    __extends(App, _super);
    function App() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    App.prototype.listen = function (source) {
        var _this = this;
        source
            .map(function (_a) {
            var req = _a.req, res = _a.res;
            _this._processMiddleware(req, res, function (error) {
                if (error) {
                    if (_this.errorHandler) {
                        _this.errorHandler(error, req, res);
                    }
                    else {
                        res.status(500).send({ message: error.message });
                    }
                }
                else {
                    if (!res.finished) {
                        res.status(404).send({ message: 'NotFound!' });
                    }
                }
            });
        })
            .subscribe();
    };
    return App;
}(Router));
exports.App = App;
/**
 * Decode param value.
 *
 * @param {string} val
 * @return {string}
 * @private
 */
function decodeParam(val) {
    if (typeof val !== 'string' || val.length === 0) {
        return val;
    }
    try {
        return decodeURIComponent(val);
    }
    catch (err) {
        if (err instanceof URIError) {
            err.message = 'Failed to decode param \'' + val + '\'';
            // err.status = err.statusCode = 400;
        }
        throw err;
    }
}
// const app = new App();
// const router = new Router();
// app.use('/', router);
// app.get('/', function(req, next) {
//   console.log('app get / handler');
// //   return next('stop');
// //   next();
// //   return Observable.throw('hady');
//   return Observable.empty();
// });
// app.get('/', function(req) {
//   console.log('app get / handler - 2');
//   return Observable.empty();
// });
// app.listen(subject);
// trigger({ method: 'GET', route: '/' });
// console.log('hey');
