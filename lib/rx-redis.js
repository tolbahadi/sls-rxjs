"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var rxjs_1 = require("@reactivex/rxjs");
var logger_1 = require("./logger");
var lodash_1 = require("lodash");
var redis = require("redis");
var Redis = (function () {
    function Redis() {
        this.log = false;
    }
    Redis.prototype.connect = function (log) {
        if (log === void 0) { log = false; }
        this.client = redis.createClient({
            host: process.env['REDIS'],
        });
        this.log = false;
        this.client.on('error', function (error) {
            logger_1.logger.error('RedisError', { error: error });
        });
        return this.client;
    };
    Redis.prototype.getClient = function () {
        return this.client;
    };
    Redis.prototype.quit = function () {
        this.client && this.client.quit();
        delete this.client;
    };
    Redis.prototype.method = function (name) {
        var _this = this;
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        return rxjs_1.Observable.create(function (observer) {
            var command = name + ' ' + (args || []).join(' ');
            if (_this.log) {
                logger_1.logger.info('Redis.Method', { command: command });
            }
            (_a = _this.client)[name].apply(_a, args.concat([function (error, result) {
                    if (error) {
                        logger_1.logger.error('RedisMethodError', { command: command, error: error });
                        observer.error(error);
                    }
                    else {
                        observer.next(result);
                        observer.complete();
                    }
                }]));
            var _a;
        });
    };
    Redis.prototype.multi = function () {
        var _this = this;
        var multi = this.client.multi();
        var multiObservable = lodash_1.assign(multi, {
            execObservable: function () {
                var commands = lodash_1.map(multi.queue, function (item) { return item ? item.command + ' ' + item.args.join(' ') : null; });
                if (_this.log) {
                    logger_1.logger.info('Redis.ExecObservable', { commands: commands });
                }
                return rxjs_1.Observable.create(function (observer) {
                    multi.exec(function (error, result) {
                        if (error) {
                            logger_1.logger.error('ExecObservableFailed', { commands: commands, error: error });
                            observer.error(error);
                        }
                        else {
                            observer.next(result);
                            observer.complete();
                        }
                    });
                });
            },
        });
        return multiObservable;
    };
    return Redis;
}());
exports.Redis = Redis;
function parseHash(hash) {
    if (!hash) {
        return;
    }
    var obj = {};
    lodash_1.each(hash, function (value, key) {
        try {
            obj[key] = JSON.parse(value);
        }
        catch (error) {
            logger_1.logger.error('failed parsing value', { key: key, value: value, error: error });
        }
    });
    return obj;
}
exports.parseHash = parseHash;
function serializeToHash(obj) {
    if (!obj) {
        return;
    }
    var hash = {};
    lodash_1.each(obj, function (value, key) {
        hash[key] = JSON.stringify(value);
    });
    return hash;
}
exports.serializeToHash = serializeToHash;
