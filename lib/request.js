'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
var lodash_1 = require("lodash");
var cookie = require("cookie");
var HttpRequest = (function () {
    function HttpRequest(event) {
        var _this = this;
        this.query = {};
        this.params = {};
        this.headers = {};
        this.cookies = {};
        this.body = {};
        // console.log(JSON.stringify(event, null, 2))
        lodash_1.assign(this, lodash_1.pick(event, [
            'resource', 'path', 'httpMethod',
        ]));
        this.data = {};
        this.context = event.requestContext;
        lodash_1.each(['query', 'params'], function (name) {
            var value = name === 'query' ? event.queryStringParameters : event.pathParameters;
            var start = {};
            if (value) {
                var parsed = lodash_1.reduce(value, function (memo, val, key) {
                    memo[key] = decodeURI(val);
                    return memo;
                }, start);
                name === 'query' ? _this.query = parsed : _this.params = parsed;
            }
        });
        if (event.headers) {
            this.headers = {};
            lodash_1.each(event.headers, function (value, key) {
                _this.headers[key.toLocaleLowerCase()] = value;
            });
        }
        if (this.headers['content-type'] === 'application/json') {
            try {
                this.body = JSON.parse(event.body);
            }
            catch (err) {
                err = null;
            }
        }
        else {
            this.body = event.body;
        }
        if (this.headers['cookie']) {
            this.cookies = cookie.parse(this.headers['cookie']);
        }
        if (event.path) {
            this.path = decodeURI(event.path);
        }
        this.lang = getLang(this);
    }
    HttpRequest.prototype.set = function (key, val) {
        this.data[key] = val;
    };
    HttpRequest.prototype.get = function (key) {
        return this.data[key];
    };
    return HttpRequest;
}());
exports.HttpRequest = HttpRequest;
var langs = {
    ar: 'ar',
    fr: 'fr',
};
function getLang(request) {
    var lang = '';
    if (request.query.lang && langs[request.query.lang.toLowerCase()]) {
        lang = langs[request.query.lang.toLowerCase()];
    }
    else if (request.cookies.lang && langs[request.cookies.lang.toLowerCase()]) {
        lang = langs[request.cookies.lang.toLowerCase()];
    }
    return lang;
}
