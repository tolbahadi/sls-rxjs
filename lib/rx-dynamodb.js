"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AWS = require("aws-sdk");
var lodash_1 = require("lodash");
var rxjs_1 = require("@reactivex/rxjs");
var config = {
    region: 'us-east-1',
    apiVersion: '2012-08-10',
};
if (process.env.LOCAL_DYNAMO_ENDPOINT) {
    config.endpoint = process.env.LOCAL_DYNAMO_ENDPOINT;
}
var service = new AWS.DynamoDB(config);
var dynamodb = new AWS.DynamoDB.DocumentClient({
    service: service,
});
function buildExpression(expressions) {
    var result = {
        expression: '',
        names: {},
        values: {},
    };
    var expressionParts = [];
    if (!lodash_1.isArray(expressions)) {
        expressions = lodash_1.map(expressions, function (value, key) { return ({ key: key, value: value }); });
    }
    lodash_1.each(expressions, function (expression) {
        var attributeName = "#" + expression.key + "Attr";
        var attributeValue = ":" + expression.key + "Value";
        result.names[attributeName] = expression.key;
        result.values[attributeValue] = expression.value;
        if (expression.fn) {
            expressionParts.push(expression.fn + "(" + attributeName + ", " + attributeValue + ")");
        }
        else {
            expressionParts.push(attributeName + " " + (expression.comp || '=') + " " + attributeValue);
        }
    });
    result.expression += expressionParts.join(', ');
    return result;
}
var $get = rxjs_1.Observable.bindNodeCallback(function (params, cb) {
    return dynamodb.get(params, cb);
});
function get(tableName, key, options) {
    var params = lodash_1.assign({
        TableName: tableName,
        Key: key,
    }, options);
    return $get(params);
}
exports.get = get;
var $delete = rxjs_1.Observable.bindNodeCallback(function (params, cb) {
    return dynamodb.delete(params, cb);
});
function deleteItem(tableName, key, options) {
    var params = lodash_1.assign({
        TableName: tableName,
        Key: key,
    }, options);
    return $delete(params);
}
exports.deleteItem = deleteItem;
var $put = rxjs_1.Observable.bindNodeCallback(function (params, cb) {
    return dynamodb.put(params, cb);
});
function put(tableName, item, options) {
    return $put(lodash_1.assign({
        TableName: tableName,
        Item: item,
    }, options));
}
exports.put = put;
var $update = rxjs_1.Observable.bindNodeCallback(function (params, cb) {
    return dynamodb.update(params, cb);
});
function update(tableName, item, attrs, options) {
    var expressionSpec = buildExpression(attrs);
    var params = lodash_1.assign({
        TableName: tableName,
        Key: item,
        UpdateExpression: "set " + expressionSpec.expression,
        ExpressionAttributeNames: expressionSpec.names,
        ExpressionAttributeValues: expressionSpec.values,
    }, options);
    return $update(params);
}
exports.update = update;
;
var $query = rxjs_1.Observable.bindNodeCallback(function (params, cb) {
    return dynamodb.query(params, cb);
});
function query(tableName, expressions, options, extraOptions) {
    if (options === void 0) { options = {}; }
    var expressionSpec = buildExpression(expressions);
    var params = {
        TableName: tableName,
        KeyConditionExpression: expressionSpec.expression,
        ExpressionAttributeNames: expressionSpec.names,
        ExpressionAttributeValues: expressionSpec.values,
        ScanIndexForward: options.scanForward || false,
        IndexName: options.indexName,
    };
    if (options.limit) {
        params.Limit = options.limit;
    }
    if (options.startKey) {
        params.ExclusiveStartKey = options.startKey;
    }
    lodash_1.assign(params, extraOptions);
    return $query(params);
}
exports.query = query;
var $scan = rxjs_1.Observable.bindNodeCallback(function (params, cb) {
    return dynamodb.scan(params, cb);
});
function scan(tableName, expressions, options, extraOptions) {
    var expressionSpec = buildExpression(expressions);
    var params = {
        TableName: tableName,
        IndexName: options.indexName,
    };
    if (expressionSpec.expression) {
        params.FilterExpression = expressionSpec.expression;
        params.ExpressionAttributeNames = expressionSpec.names;
        params.ExpressionAttributeValues = expressionSpec.values;
    }
    if (options.indexName) {
        params.IndexName = options.indexName;
    }
    if (options.startKey) {
        params.ExclusiveStartKey = options.startKey;
    }
    if (options.limit) {
        params.Limit = options.limit;
    }
    lodash_1.assign(params, extraOptions);
    return $scan(params);
}
exports.scan = scan;
var TableClass = (function () {
    function TableClass(tableName) {
        this.tableName = '';
        this.tableName = tableName;
    }
    TableClass.prototype.get = function (key, options) {
        return get(this.tableName, key, options);
    };
    TableClass.prototype.delete = function (key, options) {
        return deleteItem(this.tableName, key, options);
    };
    TableClass.prototype.put = function (item, options) {
        return put(this.tableName, item, options);
    };
    TableClass.prototype.update = function (item, attrs, options) {
        return update(this.tableName, item, attrs, options);
    };
    TableClass.prototype.query = function (expression, options, extraOptions) {
        return query(this.tableName, expression, options, extraOptions);
    };
    TableClass.prototype.scan = function (expression, options, extraOptions) {
        return scan(this.tableName, expression, options, extraOptions);
    };
    return TableClass;
}());
exports.TableClass = TableClass;
function table(tableName) {
    return new TableClass(tableName);
}
exports.table = table;
