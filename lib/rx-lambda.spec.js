"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var rx_lambda_1 = require("./rx-lambda");
describe('rx-lambda', function () {
    describe('getHandler', function () {
        it('returns observer', function (done) {
            var _a = rx_lambda_1.getHandler(), lambdaFn = _a.lambdaFn, $subject = _a.$subject;
            var event = {};
            var context = {};
            var callback = function () { return; };
            $subject.subscribe(function (value) {
                expect(value).toEqual({
                    event: event,
                    context: context,
                    callback: callback,
                });
                done();
            });
            lambdaFn(event, context, callback);
        });
    });
});
