"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var aws_sdk_1 = require("aws-sdk");
var rxjs_1 = require("@reactivex/rxjs");
var sqs = new aws_sdk_1.SQS({
    apiVersion: '2012-11-05',
    region: 'us-east-1',
});
var RxSQS = (function () {
    function RxSQS() {
    }
    RxSQS.prototype.sendMessage = function (data, delay) {
        var _this = this;
        if (delay === void 0) { delay = 0; }
        return rxjs_1.Observable.create(function (observer) {
            sqs.sendMessage({
                DelaySeconds: delay,
                MessageBody: JSON.stringify(data),
                QueueUrl: _this.queuUrl,
            }, function (error, result) {
                if (error) {
                    observer.error(error);
                }
                else {
                    observer.next(result);
                    observer.complete();
                }
            });
        });
    };
    RxSQS.prototype.receiveMessage = function (max) {
        var _this = this;
        if (max === void 0) { max = 1; }
        return rxjs_1.Observable.create(function (observer) {
            sqs.receiveMessage({
                QueueUrl: _this.queuUrl,
                MaxNumberOfMessages: max,
            }, function (error, result) {
                if (error) {
                    observer.error(error);
                }
                else {
                    if (result.Messages) {
                        result.Messages.forEach(function (message) {
                            observer.next({
                                data: JSON.parse(message.Body),
                                message: message,
                            });
                        });
                    }
                    observer.complete();
                }
            });
        });
    };
    RxSQS.prototype.deleteMessage = function (receiptHandler) {
        var _this = this;
        return rxjs_1.Observable.create(function (observer) {
            sqs.deleteMessage({
                QueueUrl: _this.queuUrl,
                ReceiptHandle: receiptHandler,
            }, function (error, result) {
                if (error) {
                    observer.error(error);
                }
                else {
                    observer.next(result);
                    observer.complete();
                }
            });
        });
    };
    return RxSQS;
}());
exports.RxSQS = RxSQS;
