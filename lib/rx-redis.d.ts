import { Observable } from '@reactivex/rxjs';
import * as redis from 'redis';
export declare type redisCommand = 'get' | 'set' | 'setnx' | 'setex' | 'append' | 'strlen' | 'del' | 'exists' | 'setbit' | 'getbit' | 'setrange' | 'getrange' | 'substr' | 'incr' | 'decr' | 'mget' | 'rpush' | 'lpush' | 'rpushx' | 'lpushx' | 'linsert' | 'rpop' | 'lpop' | 'brpop' | 'brpoplpush' | 'blpop' | 'llen' | 'lindex' | 'lset' | 'lrange' | 'ltrim' | 'lrem' | 'rpoplpush' | 'sadd' | 'srem' | 'smove' | 'sismember' | 'scard' | 'spop' | 'srandmember' | 'sinter' | 'sinterstore' | 'sunion' | 'sunionstore' | 'sdiff' | 'sdiffstore' | 'smembers' | 'zadd' | 'zincrby' | 'zrem' | 'zremrangebyscore' | 'zremrangebyrank' | 'zunionstore' | 'zinterstore' | 'zrange' | 'zrangebyscore' | 'zrevrangebyscore' | 'zcount' | 'zrevrange' | 'zcard' | 'zscore' | 'zrank' | 'zrevrank' | 'hset' | 'hsetnx' | 'hget' | 'hmset' | 'hmget' | 'hincrby' | 'hincrbyfloat' | 'hdel' | 'hlen' | 'hkeys' | 'hvals' | 'hgetall' | 'hexists' | 'incrby' | 'decrby' | 'getset' | 'mset' | 'msetnx' | 'randomkey' | 'select' | 'move' | 'rename' | 'renamenx' | 'expire' | 'expireat' | 'keys' | 'dbsize' | 'auth' | 'ping' | 'echo' | 'save' | 'bgsave' | 'bgrewriteaof' | 'shutdown' | 'lastsave' | 'type' | 'multi' | 'exec' | 'discard' | 'sync' | 'flushdb' | 'flushall' | 'sort' | 'info' | 'monitor' | 'ttl' | 'persist' | 'slaveof' | 'debug' | 'config' | 'subscribe' | 'unsubscribe' | 'psubscribe' | 'punsubscribe' | 'publish' | 'watch' | 'unwatch' | 'cluster' | 'restore' | 'migrate' | 'dump' | 'object' | 'client' | 'eval' | 'evalsha' | 'sscan';
export interface RedisMultiObservable extends redis.Multi {
    execObservable(): Observable<any>;
}
export declare class Redis {
    client: redis.RedisClient;
    log: Boolean;
    connect(log?: boolean): redis.RedisClient;
    getClient(): redis.RedisClient;
    quit(): void;
    method(name: redisCommand, ...args: any[]): Observable<any>;
    multi(): redis.Multi & {
        execObservable: () => Observable<any>;
    };
}
export declare function parseHash(hash: any): any;
export declare function serializeToHash(obj: any): any;
