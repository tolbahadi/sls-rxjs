import { Observable } from '@reactivex/rxjs';
import { HttpRequest } from './request';
import { HttpResponse } from './response';
import * as pathToRegexp from 'path-to-regexp';
export interface RouteHandlerCallback {
    (req: HttpRequest, res: HttpResponse, next: Function, param?: string): void;
}
export declare type Method = 'GET' | 'POST' | 'PUT' | 'DELETE' | 'ANY';
export declare type RouteHandler = RouteHandlerCallback | Router;
export interface Middleware {
    method: Method;
    path: string;
    handler: RouteHandler;
    regex: RegExp;
    keys: pathToRegexp.Key[];
}
export declare class Router {
    handlers: Middleware[];
    mountPrefix: string;
    errorHandler: (error: any, req: HttpRequest, res: HttpResponse) => void;
    addHandler(method: Method, path: string | RouteHandler, handler?: RouteHandler): this;
    error(error: any, req: HttpRequest, res: HttpResponse): void;
    get(path: string | RouteHandler, handler?: RouteHandler): this;
    put(path: string | RouteHandler, handler?: RouteHandler): this;
    post(path: string | RouteHandler, handler?: RouteHandler): this;
    delete(path: string | RouteHandler, handler?: RouteHandler): this;
    all(path: string | RouteHandler, handler?: RouteHandler): this;
    use(path: string | RouteHandler, handler?: RouteHandler): this;
    _shouldCallMiddleware(req: HttpRequest, middleware: Middleware): boolean;
    _callMiddleware(req: HttpRequest, res: HttpResponse, middleware: Middleware, next: Function): void;
    _processMiddleware(req: HttpRequest, res: HttpResponse, next: Function): any;
}
export declare class App extends Router {
    listen(source: Observable<{
        req: HttpRequest;
        res: HttpResponse;
    }>): void;
}
