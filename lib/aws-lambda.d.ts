export interface ILambdaHttpResult {
    body: string;
    headers?: {
        [index: string]: string;
    };
    statusCode?: number;
}
export interface LambdaCallback {
    (error?: Error, result?: any): void;
}
export interface LambdaHttpCallback {
    (error?: Error, result?: ILambdaHttpResult): void;
}
export interface ILambdaIdentity {
    cognitoIdentityPoolId: string;
    accountId: string;
    cognitoIdentityId: string;
    caller: string;
    apiKey: string;
    sourceIp: string;
    cognitoAuthenticationType: string;
    cognitoAuthenticationProvider: string;
    userArn: string;
    userAgent: string;
    user: string;
}
export interface ILambdaHttpEventContext {
    accountId: string;
    resourceId: string;
    stage: string;
    requestId: string;
    identity: ILambdaIdentity;
    resourcePath: string;
    httpMethod: string;
    apiId: string;
}
export interface ILambdaHttpEvent {
    queryStringParameters?: {
        [index: string]: string;
    };
    pathParameters?: {
        [index: string]: string;
    };
    body?: any;
    headers?: {
        [index: string]: string;
    };
    lang: string;
    resource: string;
    path: string;
    httpMethod: string;
    requestContext: ILambdaHttpEventContext;
    stageVariables: {
        [index: string]: string;
    };
}
