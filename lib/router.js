"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var rxjs_1 = require("@reactivex/rxjs");
var lodash_1 = require("lodash");
var pathToRegexp = require("path-to-regexp");
var Router = (function () {
    function Router(subject) {
        if (!subject) {
            this.subject = new rxjs_1.Subject();
        }
        else {
            this.subject = subject;
        }
    }
    Router.prototype.connect = function (source) {
        var subject = this.subject;
        // source.subscribe(
        //   console.log.bind(console),
        //   console.log.bind(console)
        //   // console.log.bind(console),
        // );
        source.subscribe(subject.next.bind(subject), subject.error.bind(subject), subject.complete.bind(subject));
    };
    Router.prototype.subscribe = function () {
        this.subject.subscribe.apply(this.subject, arguments);
    };
    Router.prototype.use = function (pathOrHandlerOrRouter, handler) {
        var path = lodash_1.isString(pathOrHandlerOrRouter) ? pathOrHandlerOrRouter : '';
        var router = pathOrHandlerOrRouter instanceof Router ? pathOrHandlerOrRouter : null;
        handler = lodash_1.isFunction(pathOrHandlerOrRouter) ? pathOrHandlerOrRouter : handler;
        var ob = this.subject;
        if (path) {
            var pathRegex_1 = pathToRegexp(path);
            ob = this.subject.filter(function (_a) {
                var req = _a.req, res = _a.res;
                return pathRegex_1.test(req.path);
            });
        }
        if (handler) {
            ob = ob.flatMap(function (_a) {
                var req = _a.req, res = _a.res;
                return handler({ req: req, res: res }).mapTo({ req: req, res: res });
            });
        }
        else {
            router.connect(ob);
            return router;
            // throw 'Not supported!';
            // ob.share().subscribe(router.subject);
            // ob = ob.flatMap(router.subject);
        }
        return new Router(ob);
    };
    Router.prototype.method = function (method, path, handler) {
        return new Router(this.subject.filter(function (_a) {
            var req = _a.req, res = _a.res;
            return req.httpMethod.toLocaleLowerCase() === method && pathToRegexp(path).test(req.path);
        })
            .flatMap(function (arg) { return handler(arg).mapTo(arg); }));
    };
    Router.prototype.get = function (path, handler) {
        return this.method('get', path, handler);
    };
    Router.prototype.post = function (path, handler) {
        return this.method('post', path, handler);
    };
    Router.prototype.put = function (path, handler) {
        return this.method('put', path, handler);
    };
    Router.prototype.delete = function (path, handler) {
        return this.method('delete', path, handler);
    };
    Router.prototype.option = function (path, handler) {
        return this.method('option', path, handler);
    };
    Router.prototype.head = function (path, handler) {
        return this.method('head', path, handler);
    };
    return Router;
}());
exports.Router = Router;
