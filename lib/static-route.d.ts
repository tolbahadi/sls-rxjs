import { HttpRequest } from './request';
import { HttpResponse } from './response';
export declare function staticRouteHandler(root: string, shouldCache: (path: string) => boolean): (req: HttpRequest, res: HttpResponse) => void;
