import { ILambdaHttpEventContext, ILambdaHttpEvent } from './aws-lambda';
export interface StringDict {
    [index: string]: string;
}
export declare class HttpRequest {
    query: StringDict;
    params: StringDict;
    headers: StringDict;
    cookies: StringDict;
    lang: string;
    resource: string;
    path: string;
    httpMethod: string;
    context: ILambdaHttpEventContext;
    body: any;
    data: {
        [key: string]: any;
    };
    constructor(event: ILambdaHttpEvent);
    set(key: string, val: any): void;
    get(key: string): any;
}
